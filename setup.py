import setuptools

import saxo

packages = setuptools.find_packages(
    exclude=["saxo.tests", "saxo.tests.integration", "saxo.tests.unit"]
)
requires = [
    "requests",
    "jwcrypto",
    "pyjwt",
    "cryptography",
    "pyopenssl",
    "websocket-client",
]


setuptools.setup(
    name="saxo",
    version=saxo.__version__,
    description="Saxo Open API Python library",
    author="Denis Volokh",
    author_email="denis.volokh@noviscient.com",
    packages=packages,
    install_requires=requires,
)
