import json
import sys

import requests

from saxo.api import SaxoAPI
from saxo.endpoints.instruments import OptionsDetails
from saxo.endpoints.orders import CancelOrder, PlaceOrder, UpdateOrder
from saxo.endpoints.positions import NetPositions, Positions
from saxo.exceptions import BadRequest, SaxoAPIException, Unauthorized

url = "https://gateway.saxobank.com/sim/openapi/trade/v2/orders"
token = "	eyJhbGciOiJFUzI1NiIsIng1dCI6IkQ2QzA2MDAwMDcxNENDQTI5QkYxQTUyMzhDRUY1NkNENjRBMzExMTcifQ.eyJvYWEiOiI3Nzc3MCIsImlzcyI6Im9hIiwiYWlkIjoiOTEiLCJ1aWQiOiJjbE5JM0lHb0c1eHBFY1d2QjNGcGRnPT0iLCJjaWQiOiJjbE5JM0lHb0c1eHBFY1d2QjNGcGRnPT0iLCJpc2EiOiJGYWxzZSIsInRpZCI6IjIwODAiLCJzaWQiOiI2NjdlYTY5NmJiNDg0YTU4OWI3N2I5NjliZDdiMGZmOSIsImRnaSI6Ijg0IiwiZXhwIjoiMTU3NzQzNjAwMiJ9.DnLR_V-GyZ9bXIw92Adk3lROHprL-48aJxj8fFhHGLVR1yVqNgwgxJcc5VtiFdEJPFtGUtIMp0bzVRkhgub3QQ"
ACCOUNT_KEY = "Pw-d7jitNM-Yc6B6R5cWOg=="
CLIENT_KEY = "clNI3IGoG5xpEcWvB3Fpdg=="

# update_order = {
#     "OrderId": "82961044",
#     "Uic": 211,
#     "Amount": 12000,
#     # "BuySell": "Buy123",
#     # "AssetType": "CfdOnStock123",
#     "OrderType": "Market123",
#     "OrderDuration": {
#     	"DurationType": "DayOrder"
#     },
#     "AccountKey": "g3JbRMz2HoZvWq|mQrkVow=="
# }

new_order = {
    "Symbol": "ANF",
    "OrderPrice": 16.43,
    "Amount": 229,
    "BuySell": "SELL 111",
    "OrderType": "LIMIT",
    "OrderDuration": {"DurationType": "DayOrder"},
    "AccountKey": ACCOUNT_KEY,
    "AssetType": "CfdOnStock",
}

headers = {"Authorization": f"Bearer {token}", "Content-Type": "application/json"}
# r = requests.patch(url, headers=headers, json=order)
# print(r)


def capture():
    _class, _instance, _traceback = sys.exc_info()

    print(_class)


client = SaxoAPI(access_token=token, account_key=ACCOUNT_KEY, client_key=CLIENT_KEY)

new_order = OptionsDetails(4)
# update_order_endpoint = UpdateOrder(order=order, uic=211, asset_type="CfdOnStock", account_key="g3JbRMz2HoZvWq|mQrkVow==")
# cancel_order_endpoint = CancelOrder(order_ids="83318143")
# positions_endpoint = Positions()
# positions_endpoint = NetPositions()
try:
    positions_result = client.request(new_order)

    print(positions_result)

except BadRequest as bad_exc:
    original_saxo_error = bad_exc.args[0]
    capture()

except Unauthorized as notauth_exc:
    original_saxo_error = bad_exc.args[0]  # 'Unauthorized access, invalid access token'
    capture()

except SaxoAPIException as saxo_exc:
    capture()
    print(saxo_exc.alt_message)
    print(saxo_exc)
