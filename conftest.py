import os

import betamax
import pytest
from betamax_serializers import pretty_json

from saxo.api import SaxoAPI
from saxo.definitions.environment import Environment

betamax.Betamax.register_serializer(pretty_json.PrettyJSONSerializer)

with betamax.Betamax.configure() as config:
    config.cassette_library_dir = "saxo/tests/integration/cassettes"
    config.default_cassette_options["serialize_with"] = "prettyjson"


ACCESS_TOKEN = os.getenv("SAXO__ACCESS_TOKEN")
CLIENT_KEY = os.getenv("SAXO__CLIENT_KEY")
ACCOUNT_KEY = os.getenv("SAXO__ACCOUNT_KEY")


@pytest.fixture
def saxo_open_api_sim_client():
    """
        Returns a SaxoAPI instance for SIM environment
    """

    sim_client = SaxoAPI(
        access_token=ACCESS_TOKEN,
        client_key=CLIENT_KEY,
        account_key=ACCOUNT_KEY,
        environment=Environment.SIM,
    )

    return sim_client


# @pytest.fixture
# def saxo_open_api_live_client():
#     """
#         Returns a SaxoOpenApi instance for LIVE environment
#     """

#     access_token = os.getenv("SAXO__ACCESS_TOKEN")
#     client_key = os.getenv("SAXO__CLIENT_KEY")
#     account_key = os.getenv("SAXO__ACCOUNT_KEY")

#     live_client = SaxoOpenAPI(access_token=access_token, client_key=client_key, account_key=account_key,
#                              environment=LIVE_ENVIRONMENT)

#     return live_client
