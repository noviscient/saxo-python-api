import base64
import logging

import requests

from saxo.definitions.environment import Environment
from saxo.endpoints.apirequest import APIRequest
from saxo.exceptions import BadEnvironment, raise_for_status

from .auth import make_jwt_token

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger("saxo.api")

ENVIRONMENTS = {
    "SIM": {
        "API": "https://gateway.saxobank.com/sim",
        "ISALIVE": "https://gateway.saxobank.com/sim/openapi/port/isalive",
        "TOKEN": "https://sim.logonvalidation.net/token",
    },
    "LIVE": {
        "API": "https://gateway.saxobank.com/sim",
        "ISALIVE": "https://gateway.saxobank.com/openapi/port/isalive",
        "TOKEN": "https://live.logonvalidation.net/token",
    },
}


class SaxoAPI:
    """
        Wrapper for Saxo Open API

        See also the API documentation `here`.

        .. _here: https://www.developer.saxo/openapi/learn
    """

    def __init__(
        self,
        access_token: str,
        account_key: str = None,
        client_key: str = None,
        environment: str = Environment.Sim,
    ):

        try:
            ENVIRONMENTS[environment]

        except KeyError:
            raise BadEnvironment()

        self.environment = environment
        self.session = requests.Session()
        response = self.session.get(ENVIRONMENTS[environment]["ISALIVE"])
        raise_for_status(response)

        self._account_key = account_key
        self._client_key = client_key
        self.__access_token = None

        if access_token is None:
            raise ValueError("All methods require an access token.")

        self.access_token = access_token

    @property
    def access_token(self):
        return self.__access_token

    @access_token.setter
    def access_token(self, value):
        self.__access_token = value
        self.session.headers["Authorization"] = f"Bearer {self.__access_token}"

    def __make_request(
        self,
        url: str,
        method: str,
        query_params: dict = None,
        data_params: dict = None,
        headers: dict = None,
    ):

        logging.debug(f"[------] Data Params: {data_params}")
        logging.debug(f"[------] Query Params: {query_params}")
        logging.debug(f"[------] Method: {method}")

        if headers is None:
            headers = {}

        response = None

        if method == "GET":
            response = self.session.get(url=url, params=query_params)

        elif method == "POST":
            self.session.headers.update({"Content-Type": "application/json"})
            # response = self.session.post(url=url, data=json.dumps(data_params))
            response = self.session.post(url=url, json=data_params)

        elif method == "DELETE":
            response = self.session.delete(url=url, params=query_params)

        elif method == "PATCH":
            response = self.session.patch(url=url, json=data_params)

        else:
            raise ValueError("Method not found!")

        return response

    def request(self, endpoint: APIRequest):

        url = f"{ENVIRONMENTS[self.environment]['API']}{endpoint}"

        try:
            required_query_params = getattr(endpoint, "REQUIRED")

            if "ClientKey" in required_query_params.keys():
                required_query_params["ClientKey"] = self._client_key

            if "AccountKey" in required_query_params.keys():
                required_query_params["AccountKey"] = self._account_key

        except AttributeError:
            required_query_params = None

        try:
            query_params = getattr(endpoint, "params")

        except AttributeError:
            query_params = {}

        if required_query_params:
            query_params.update(required_query_params)

        try:
            data_params = getattr(endpoint, "data")

            if "Arguments" in data_params:
                data_params["Arguments"]["AccountKey"] = self._account_key
                data_params["Arguments"]["ClientKey"] = self._client_key

        except AttributeError:
            data_params = {}

        response = self.__make_request(
            url=url,
            method=endpoint.method,
            data_params=data_params,
            query_params=query_params,
        )

        raise_for_status(response)

        content = response.json()
        endpoint.response = response
        endpoint.status_code = response.status_code

        return content

    def refresh_token(self, app_key: str, app_secret: str, refresh_token: str) -> dict:
        app_key_secret = f"{app_key}:{app_secret}"
        app_key_secret_encoded = base64.b64encode(app_key_secret.encode())

        headers = {
            "authorization": f"Basic {app_key_secret_encoded.decode('utf-8')}",
            "content-type": "application/x-www-form-urlencoded",
        }

        params = {"grant_type": "refresh_token", "refresh_token": refresh_token}

        response = requests.request(
            "POST",
            ENVIRONMENTS[self.environment]["TOKEN"],
            headers=headers,
            params=params,
        )
        raise_for_status(response)

        return response.json()

    def create_token(
        self,
        app_key: str,
        app_secret: str,
        app_url: str,
        auth_url: str,
        saxo_client_id: str,
        path_to_cert: str,
        cert_password: str,
    ) -> dict:
        logger.debug(f"[+] Create Access Token!")

        app_key_secret = f"{app_key}:{app_secret}"
        app_key_secret_encoded = base64.b64encode(app_key_secret.encode())

        headers = {
            "authorization": f"Basic {app_key_secret_encoded.decode('utf-8')}",
            "content-type": "application/x-www-form-urlencoded",
        }

        params = {
            "grant_type": "urn:saxobank:oauth:grant-type:personal-jwt",
            "assertion": make_jwt_token(
                app_key=app_key,
                app_url=app_url,
                auth_url=auth_url,
                client_id=saxo_client_id,
                path_to_cert=path_to_cert,
                cert_password=cert_password,
            ),
        }

        response = requests.request(
            "POST",
            ENVIRONMENTS[self.environment]["TOKEN"],
            headers=headers,
            params=params,
        )
        raise_for_status(response)

        return response.json()
