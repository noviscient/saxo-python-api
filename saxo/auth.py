from datetime import datetime, timedelta

import jwt
from OpenSSL.crypto import (
    FILETYPE_PEM,
    dump_certificate,
    dump_privatekey,
    load_certificate,
    load_pkcs12,
)


def make_jwt_token(
    app_key: str,
    app_url: str,
    auth_url: str,
    client_id: str,
    path_to_cert: str,
    cert_password: str,
) -> bytes:

    with open(path_to_cert, "rb") as f:
        c = f.read()

    p = load_pkcs12(c, cert_password)

    certificate = p.get_certificate()
    private_key = p.get_privatekey()

    private_key_string = dump_privatekey(FILETYPE_PEM, private_key)
    certificate_string = dump_certificate(FILETYPE_PEM, certificate)

    cert = load_certificate(FILETYPE_PEM, certificate_string)
    sha1_fingerprint = cert.digest("sha1")
    sha1_fingerprint = sha1_fingerprint.decode("utf-8").lower().replace(":", "")
    print(sha1_fingerprint)

    header = {"x5t": sha1_fingerprint, "alg": "RS256"}

    message = {
        "iss": app_key,
        "sub": client_id,
        "exp": datetime.utcnow() + timedelta(seconds=1200),
        "aud": auth_url,
        "spurl": app_url,
    }

    jwt_token = jwt.encode(
        payload=message, key=private_key_string, algorithm="RS256", headers=header
    )
    # jwt_token = jwt_token.decode("utf-8")

    return jwt_token
