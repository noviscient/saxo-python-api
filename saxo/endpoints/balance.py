from .apirequest import APIRequest


class Balance(APIRequest):
    """
            Get balance data for a client, account group or an account
            Docs: https://www.developer.saxo/openapi/referencedocs/endpoint?apiVersion=v1&serviceGroup=portfolio&service=balances&endpoint=getbalance&hash=2e861b215c4f07c2ec49511cb5ed8685

        Returns:
            Dictionary object containing response code and data returned from API.

        """

    ENDPOINT = "/openapi/port/v1/balances"
    REQUIRED = {"ClientKey": None, "AccountKey": None}

    def __init__(self):
        super(Balance, self).__init__(endpoint=self.ENDPOINT)


class BalanceSubscription(APIRequest):
    """
            Sets up a subscription and returns an initial snapshot of a balance.
            Docs https://www.developer.saxo/openapi/referencedocs/endpoint?apiVersion=v1&serviceGroup=portfolio&service=balances&endpoint=addactivesubscription&hash=aec00bc9b272be3c599e8347929f9a2a

        Returns:
            Dictionary object containing response code and data returned from API.

        """

    METHOD = "POST"
    ENDPOINT = "/openapi/port/v1/balances/subscriptions"

    def __init__(self, context_id: str, reference_id: str):
        super(BalanceSubscription, self).__init__(
            endpoint=self.ENDPOINT, method=self.METHOD
        )

        self.data = {
            "ContextId": context_id,
            "ReferenceId": reference_id,
            "Arguments": {"ClientKey": None, "AccountKey": None},
        }
