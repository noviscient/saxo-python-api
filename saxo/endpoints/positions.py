from .apirequest import APIRequest


class Positions(APIRequest):
    """
            Get positions for the logged-in client
            Docs: https://www.developer.saxo/openapi/referencedocs/endpoint?apiVersion=v1&serviceGroup=portfolio&service=positions&endpoint=getpositions&hash=b6d549a50a5f35244806aaa0554d6eae

        Returns:
            Dictionary object containing response code and data returned from API.

        """

    ENDPOINT = "/openapi/port/v1/positions/"
    REQUIRED = {"ClientKey": None, "AccountKey": None}

    def __init__(self):
        super(Positions, self).__init__(endpoint=self.ENDPOINT)


class NetPositions(APIRequest):
    """
            Get net positions for a client, account group or account
            Docs: https://www.developer.saxo/openapi/referencedocs/endpoint?apiVersion=v1&serviceGroup=portfolio&service=netpositions&endpoint=getnetpositions&hash=0de32138dbf1e57bcf40142b61661748

        Returns:
            Dictionary object containing response code and data returned from API.

        """

    ENDPOINT = "/openapi/port/v1/netpositions/"
    REQUIRED = {"ClientKey": None, "AccountKey": None}

    def __init__(self):
        super(NetPositions, self).__init__(endpoint=self.ENDPOINT)


class PositionsSubscription(APIRequest):
    """
            Sets up a subscription and returns an initial snapshot of list of positions specified by the parameters in the request.
            Docs: https://www.developer.saxo/openapi/referencedocs/endpoint?apiVersion=v1&serviceGroup=portfolio&service=positions&endpoint=addactivesubscription&hash=ccdff431c0efe7f9392a80239f7955d6

        Returns:
            Dictionary object containing response code and data returned from API.

        """

    METHOD = "POST"
    ENDPOINT = "/openapi/port/v1/positions/subscriptions/"

    def __init__(self, context_id: str, reference_id: str):
        super(PositionsSubscription, self).__init__(
            endpoint=self.ENDPOINT, method=self.METHOD
        )

        self.data = {
            "ContextId": context_id,
            "ReferenceId": reference_id,
            "Format": "application/json",
        }


class NetPositionsSubscription(APIRequest):
    """
            Sets up a subscription and returns an initial snapshot of list of positions specified by the parameters in the request.
            Docs: https://www.developer.saxo/openapi/referencedocs/endpoint?apiVersion=v1&serviceGroup=portfolio&service=netpositions&endpoint=addactivesubscription&hash=acbe15412d603d0b3971695cb0bb5312

        Returns:
            Dictionary object containing response code and data returned from API.

        """

    METHOD = "POST"
    ENDPOINT = "/openapi/port/v1/netpositions/subscriptions/"

    def __init__(self, context_id: str, reference_id: str):
        super(NetPositionsSubscription, self).__init__(
            endpoint=self.ENDPOINT, method=self.METHOD
        )

        self.data = {
            "ContextId": context_id,
            "ReferenceId": reference_id,
            "Format": "application/json",
        }
