from .basepayload import BasePayload


class MarketStockOrder(BasePayload):
    def __init__(self):
        super(MarketStockOrder, self).__init__()


class MarketFxOrder(BasePayload):
    def __init__(self, amount: int, buysell: str, uic: int):
        super(MarketFxOrder, self).__init__()

        self._data.update({"OrderType": "Market"})
        self._data.update({"Amount": amount})
        self._data.update({"BuySell": buysell})
        self._data.update({"Uic": uic})
        self._data.update({"AssetType": "FxSpot"})
        self._data.update({"Duration": {"DurationType": "GoodTillCancel"}})
