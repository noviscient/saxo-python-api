from .apirequest import APIRequest


class Accounts(APIRequest):
    """
            Get all accounts under a particular client to which the logged in user belongs
            Docs: https://www.developer.saxo/openapi/referencedocs/endpoint?apiVersion=v1&serviceGroup=portfolio&service=accounts&endpoint=getaccounts&hash=af56e3512758f8125dc6e5493d93c019

        Returns:
            Dictionary object containing response code and data returned from API.

    """

    ENDPOINT = "/openapi/port/v1/accounts/me"

    def __init__(self):
        super(Accounts, self).__init__(endpoint=self.ENDPOINT)
