class APIRequest(object):

    METHOD = "GET"

    def __init__(self, endpoint: str, method: str = "GET"):
        self._endpoint = endpoint

        self._response = None
        self._status_code = None

        self.method = method

    @property
    def response(self):
        return self._response

    @response.setter
    def response(self, value):
        self._response = value

    @property
    def status_code(self):
        return self._status_code

    @status_code.setter
    def status_code(self, value):
        self._status_code = value

    def __str__(self):
        return self._endpoint
