from .apirequest import APIRequest


class Orders(APIRequest):
    """
            Get all open orders for the client to which the logged in user belongs
            Docs: https://www.developer.saxo/openapi/referencedocs/endpoint?apiVersion=v1&serviceGroup=portfolio&service=orders&endpoint=getopenorders&hash=cae24349737ea6fef4f0e6d9c477794e

        Returns:
            Dictionary object containing response code and data returned from API.

        """

    ENDPOINT = "/openapi/port/v1/orders/"
    REQUIRED = {"ClientKey": None, "AccountKey": None}

    def __init__(self):
        super(Orders, self).__init__(endpoint=self.ENDPOINT)


class OrderActivities(APIRequest):
    """
        To DO: added description and docs url

    """

    ENDPOINT = "/openapi/cs/v1/audit/orderactivities"
    REQUIRED = {"ClientKey": None, "AccountKey": None}

    def __init__(self, account_id: str, *args, **kwargs):
        super(OrderActivities, self).__init__(endpoint=self.ENDPOINT)
        
        self.params = {"AccountId": account_id}
        self.params.update(kwargs)


class BasicOrder(APIRequest):
    def __init__(self, *args, **kwargs):
        super(BasicOrder, self).__init__(*args, **kwargs)


class PlaceOrder(BasicOrder):
    """
        To DO: added description and docs url

    """

    METHOD = "POST"
    ENDPOINT = "/openapi/trade/v2/orders"

    def __init__(self, order: dict, *args, **kwargs):
        super(PlaceOrder, self).__init__(endpoint=self.ENDPOINT, method=self.METHOD)

        # self.data = self._update_orders_data(order, uic, asset_type, account_key)
        self.data = order


class UpdateOrder(BasicOrder):
    """
        To DO: added description and docs url

    """

    METHOD = "PATCH"
    ENDPOINT = "/openapi/trade/v2/orders"

    def __init__(self, order: dict):
        super(UpdateOrder, self).__init__(endpoint=self.ENDPOINT, method=self.METHOD)

        # self.data = self._update_orders_data(order, uic, asset_type, account_key)
        self.data = order


class CancelOrder(APIRequest):
    """
        To DO: added description and docs url

    """

    METHOD = "DELETE"
    ENDPOINT = "/openapi/trade/v2/orders/{order_ids}/"
    REQUIRED = {"AccountKey": None}

    def __init__(self, order_ids: str):
        endpoint = self.ENDPOINT.format(order_ids=order_ids)

        super(CancelOrder, self).__init__(endpoint=endpoint, method=self.METHOD)


class OrdersSubscription(APIRequest):
    """
        Sets up a subscription and returns an initial snapshot of list of orders specified by the parameters in the request.

        Docs: https://www.developer.saxo/openapi/referencedocs/endpoint?apiVersion=v1&serviceGroup=portfolio&service=orders&endpoint=addactivesubscription&hash=83e6ef44f178aad31962aa736b964799

    """

    METHOD = "POST"
    ENDPOINT = "/openapi/port/v1/orders/subscriptions"

    def __init__(self, context_id: str, reference_id: str):
        super(OrdersSubscription, self).__init__(
            endpoint=self.ENDPOINT, method=self.METHOD
        )

        self.data = {
            "ContextId": context_id,
            "ReferenceId": reference_id,
            "Format": "application/json",
        }
