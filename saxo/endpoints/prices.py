from .apirequest import APIRequest


class PricesInfoSubscription(APIRequest):
    """
            Sets up a subscription and returns an initial snapshot of an info price list specified by the parameters in the request.
            Docs: https://www.developer.saxo/openapi/referencedocs/endpoint?apiVersion=v1&serviceGroup=trading&service=info%20prices&endpoint=addsubscriptionasync&hash=38ca8a186fa551f5c2e16e9d7a25a7e2

        Returns:
            Dictionary object containing response code and data returned from API.

        """

    METHOD = "POST"
    ENDPOINT = "/openapi/trade/v1/infoprices/subscriptions"

    def __init__(self, context_id: str, reference_id: str, asset_type: str, uics: list):
        super(PricesInfoSubscription, self).__init__(
            endpoint=self.ENDPOINT, method=self.METHOD
        )

        self.data = {
            "ContextId": context_id,
            "ReferenceId": reference_id,
            "Arguments": {
                "ClientKey": None,
                "AccountKey": None,
                "AssetType": asset_type,
                "Uics": ",".join([str(x) for x in uics]),
            },
        }


class PricesSubscription(APIRequest):
    """
            Sets up an active price subscription on an instrument and returns an initial snapshot of the most recent price.
            Docs: https://www.developer.saxo/openapi/referencedocs/endpoint?apiVersion=v1&serviceGroup=trading&service=info%20prices&endpoint=getinfopriceasync

        Returns:
            Dictionary object containing response code and data returned from API.

        """

    METHOD = "POST"
    ENDPOINT = "/openapi/trade/v1/prices/subscriptions"

    def __init__(self, context_id: str, reference_id: str, asset_type: str, uic: int):
        super(PricesSubscription, self).__init__(
            endpoint=self.ENDPOINT, method=self.METHOD
        )

        self.data = {
            "ContextId": context_id,
            "ReferenceId": reference_id,
            "Arguments": {
                "ClientKey": None,
                "AccountKey": None,
                "AssetType": asset_type,
                "Uic": uic,
            },
        }
