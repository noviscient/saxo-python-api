from .apirequest import APIRequest


class SearchInstrument(APIRequest):
    """
            Search instrument
            Docs: Docs: https://www.developer.saxo/openapi/referencedocs/endpoint?apiVersion=v1&serviceGroup=referencedata&service=instruments&endpoint=getsummaries&hash=e6287542de663ed3974bf692c11d6894

        Returns:
            Dictionary object containing response code and data returned from API.

    """

    ENDPOINT = "/openapi/ref/v1/instruments/"

    def __init__(
        self,
        keywords: str,
        asset_types: str = "Stock,CfdOnStock",
        exchange_id: str = None,
    ):
        super(SearchInstrument, self).__init__(endpoint=self.ENDPOINT)

        self.params = dict(
            Keywords=keywords, ExchangeId=exchange_id, AssetTypes=asset_types
        )


class InstrumentDetails(APIRequest):
    """
            Instrument details
            Docs:
        Returns:
            Dictionary object containing response code and data returned from API.
    """

    ENDPOINT = "/openapi/ref/v1/instruments/details/{uic}/{asset_type}/"

    def __init__(self, uic: str, asset_type: str):
        endpoint = self.ENDPOINT.format(uic=uic, asset_type=asset_type)

        super(InstrumentDetails, self).__init__(endpoint=endpoint)


class OptionsDetails(APIRequest):
    """
            Options details
            Docs:
        Returns:
            Dictionary object containing response code and data returned from API.
    """

    ENDPOINT = "/openapi/ref/v1/instruments/contractoptionspaces/{option_root_id}/"

    def __init__(self, option_root_id: str, underlying_uic: int = None):
        endpoint = self.ENDPOINT.format(option_root_id=option_root_id)

        super(OptionsDetails, self).__init__(endpoint=endpoint)

        if underlying_uic:
            self.params = {"UnderlyingUic": underlying_uic}
