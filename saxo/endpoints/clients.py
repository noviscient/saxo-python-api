from .apirequest import APIRequest


class Client(APIRequest):
    """
            Get logged in client details
            Docs: https://www.developer.saxo/openapi/referencedocs/endpoint?apiVersion=v1&serviceGroup=portfolio&service=clients&endpoint=getclient&hash=1499e70934cb99a0c9e70d53f9ad8f7d

        Returns:
            Dictionary object containing response code and data returned from API.

    """

    ENDPOINT = "/openapi/port/v1/clients/me"

    def __init__(self):
        super(Client, self).__init__(endpoint=self.ENDPOINT)
