import logging
from datetime import datetime
from typing import Any, Tuple
from urllib.parse import quote_plus

import requests
from requests.cookies import RequestsCookieJar

import websocket
from saxo.api import SaxoAPI
from saxo.definitions.environment import Environment
from saxo.exceptions import BadEnvironment

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger("saxo.stream")

ENVIRONMENTS = {
    "SIM": {
        "CONNECT": "wss://streaming.saxotrader.com/sim/openapi/streaming/connection/connect",
        "NEGOTIATE": "https://streaming.saxotrader.com/sim/openapi/streaming/connection/negotiate",
    },
    "LIVE": {
        "CONNECT": "wss://streaming.saxotrader.com/openapi/streaming/connection/connect",
        "NEGOTIATE": "https://streaming.saxotrader.com/openapi/streaming/connection/negotiate",
    },
}


class SaxoStream(object):
    def __init__(
        self,
        access_token: str,
        client_key: str,
        account_key: str,
        environment: str = Environment.Sim,
    ):
        self.environment = environment
        self.client_key = client_key
        self.account_key = account_key

        try:
            ENVIRONMENTS[environment]

        except KeyError:
            raise BadEnvironment()

        self._access_token = access_token
        self._context_id = None

        self.saxo_client = SaxoAPI(
            access_token=self._access_token,
            client_key=self.client_key,
            account_key=self.account_key,
            environment=self.environment,
        )

    @property
    def context_id(self):
        if not self._context_id:
            _timestamp = (
                str(datetime.utcnow())
                .replace(" ", "_")
                .replace(":", "_")
                .replace(".", "_")
            )
            self._context_id = f"streaming_saxo_open_api_{_timestamp}"

        return self._context_id

    def _build_connect_query_params(self, connection_token: str) -> str:
        query_params = "?transport=webSockets"
        logger.debug(f"[+] Query Params: {query_params}")

        query_params += f"&connectionToken={quote_plus(connection_token)}"
        logger.debug(f"[+] Query Params: {query_params}")

        query_params += f"&authorization={quote_plus(self._access_token)}"
        logger.debug(f"[+] Query Params: {query_params}")

        query_params += f"&context={self.context_id}"
        logger.debug(f"[+] Query Params: {query_params}")

        return query_params

    def _build_negotiate_query_params(self) -> str:
        authorization = f"?authorization={self._access_token}"
        query_params = authorization
        logger.debug(f"[+] Streaming API. Negotiate URL Params: {authorization}")

        context_param = f"&context={self.context_id}"
        query_params += context_param
        logger.debug(f"[+] Streaming API. Negotiate URL Params: {context_param}")

        return query_params

    def _get_cookies(
        self, cookie_jar: RequestsCookieJar, domain: str = "streaming.saxotrader.com"
    ) -> str:
        cookie_dict = cookie_jar.get_dict(domain=domain)
        found = ["%s=%s" % (name, value) for (name, value) in cookie_dict.items()]
        return ";".join(found) + ";"

    def _negotiate(self) -> Tuple[Any, RequestsCookieJar]:
        negotiate_url = ENVIRONMENTS[self.environment]["NEGOTIATE"]
        url = f"{negotiate_url}{self._build_negotiate_query_params()}"
        logger.debug(f"[+] Negotiating: {url}")

        response = requests.get(url)
        response_data = response.json()
        connection_token = response_data["ConnectionToken"]

        logger.debug(f"[+] Negotiate Response: {response_data}")
        logger.debug(f"[+] Negotiate Response Cookies: {response.cookies}")

        return connection_token, response.cookies

    def connect(self):
        logger.debug("[+] Connecting ...")

        connection_token, cookie = self._negotiate()

        query_params = self._build_connect_query_params(connection_token)

        websocket.enableTrace(True)

        ws_url = ENVIRONMENTS[self.environment]["CONNECT"]
        url = f"{ws_url}{query_params}"

        self._ws = websocket.WebSocketApp(url=url, cookie=self._get_cookies(cookie))

        self._ws.on_message = self.on_message
        self._ws.on_error = self.on_error
        self._ws.on_close = self.on_close
        self._ws.on_open = self.on_open

        self._ws.run_forever()

    def on_message(self, message):
        raise NotImplementedError()

    def on_error(self, error):
        raise NotImplementedError()

    def on_close(self):
        raise NotImplementedError()

    def on_open(self):
        raise NotImplementedError()
