import logging

logger = logging.getLogger("restapi.exceptions")


class SaxoAPIException(Exception):
    """
        Generic error class, catches Saxo Open API response errors
        https://www.developer.saxo/openapi/learn/openapi-request-response
    """

    alt_message = "Generic exception happened"

    def __init__(self, response):
        self.response = response

        logger.debug(f"[!!!] Saxo API Exception! Response: {response.__dict__}")

        json = dict(error=None)

        try:
            json.update(error=response.json())

        except ValueError:
            json.update(error=self.alt_message)

        logger.debug(f"[!!!] Saxo API Exception: {json}")

        super(SaxoAPIException, self).__init__(json["error"])


class BadEnvironment(Exception):
    """
        Catches undefined environment, can only be SIM or LIVE
    """

    alt_message = "Incorrect environment"


class NotFound(SaxoAPIException):
    """
        Catches 404 HTTP error
    """

    alt_message = "Object not found"


class BadRequest(SaxoAPIException):
    """
        Catches Bad Request
    """

    alt_message = "Bad request"


class Unauthorized(SaxoAPIException):
    """
        Catches Unauthorized error
    """

    alt_message = "Unauthorized access, invalid access token"


class Conflict(SaxoAPIException):
    """
        Catches Conflict error
    """

    alt_message = "Conflict exception, reject duplicated order"


class BadHTTPMethod(Exception):
    """
        Catches Wrong HTTP method error
    """

    alt_message = "The HTTP method is not defined or incorrect. Should be GET, POST, DELETE or PATCH"


status_code_to_exception = {
    400: BadRequest,
    401: Unauthorized,
    404: NotFound,
    409: Conflict,
}


def raise_for_status(response):
    status_code = response.status_code

    if 400 <= status_code < 600:
        exception_class = status_code_to_exception.get(status_code, SaxoAPIException)

        raise exception_class(response)
