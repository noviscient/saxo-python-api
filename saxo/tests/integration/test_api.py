import logging
import os

import betamax
import pytest

from saxo.api import SaxoAPI
from saxo.endpoints.balance import Balance
from saxo.endpoints.clients import Client
from saxo.endpoints.instruments import InstrumentDetails, SearchInstrument
from saxo.endpoints.orders import CancelOrder, OrderActivities, PlaceOrder
from saxo.endpoints.payloads.orders import MarketFxOrder
from saxo.endpoints.positions import Positions
from saxo.exceptions import NotFound

logger = logging.getLogger("saxo.tests.integration")
logger.setLevel(level=logging.DEBUG)


ACCESS_TOKEN = os.getenv("SAXO__ACCESS_TOKEN")
CLIENT_KEY = os.getenv("SAXO__CLIENT_KEY")
ACCOUNT_KEY = os.getenv("SAXO__ACCOUNT_KEY")


class TestSaxoAPI(object):
    """
        Integration tests for the SaxoAPI object.
    """

    @pytest.fixture(autouse=True)
    def setup(self):
        """
            Create SaxoAPI and Betamax instances.
        """

        self.client = SaxoAPI(
            access_token=ACCESS_TOKEN,
            client_key=CLIENT_KEY,
            account_key=ACCOUNT_KEY,
            environment="SIM",
        )
        self.recorder = betamax.Betamax(session=self.client.session)

    @staticmethod
    def generate_cassette_name(method_name):
        """Generate cassette names for tests."""
        return "saxo_open_api__" + method_name

    # def test_invalid_access_token(self):
    #     """Verify we can handle unauthorized access."""

    #     cassette_name = self.generate_cassette_name("invalid_access_token")
    #     with self.recorder.use_cassette(cassette_name):

    #         with pytest.raises(Unauthorized) as exc_info:
    #             self.client.get_logged_in_client_details()

    def test_logged_in_client(self):
        """
            Verify we can get a details of the logged in client
        """

        client_endpoint = Client()

        cassette_name = self.generate_cassette_name("logged_in_client_details")
        with self.recorder.use_cassette(cassette_name):
            client_result = self.client.request(client_endpoint)

        assert "ClientKey" in client_result
        assert client_result["ClientKey"] == CLIENT_KEY

    def test_account_balance(self):
        """
            Verify we can get a account balance of the logged in client
        """

        balance_endpoint = Balance()

        cassette_name = self.generate_cassette_name("account_balance")
        with self.recorder.use_cassette(cassette_name):
            balance_result = self.client.request(balance_endpoint)

        assert "CashBalance" in balance_result

    def test_order_activities(self):
        """
            Verify we can get order activities
        """

        order_activities_endpoint = OrderActivities()

        cassette_name = self.generate_cassette_name("order_activities")
        with self.recorder.use_cassette(cassette_name):
            order_activities_result = self.client.request(order_activities_endpoint)

        assert "Data" in order_activities_result
        assert isinstance(order_activities_result["Data"], list)

    def test_place_fx_market_order(self):
        """
            Verify we can get order activities
        """
        payload = MarketFxOrder(amount=10000, buysell="Sell", uic=2)  # AUDJPY
        market_order_endpoint = PlaceOrder(
            order=payload.data, uic=2, asset_type="FxSpot", account_key=ACCOUNT_KEY
        )

        logger.info(
            f"[+] Place order: {market_order_endpoint.data} {market_order_endpoint.ENDPOINT} {market_order_endpoint.METHOD}"
        )

        cassette_name = self.generate_cassette_name("palce_fx_market_order")
        with self.recorder.use_cassette(cassette_name):
            place_order_result = self.client.request(market_order_endpoint)

        logger.info(f"[+] Place order result: {place_order_result}")

        assert "OrderId" in place_order_result

    def test_cancel_order(self):
        """
            Verify we can cancel order
        """

        cancel_order_endpoint = CancelOrder(order_ids="79594419999999")

        cassette_name = self.generate_cassette_name("cancel_order")
        with self.recorder.use_cassette(cassette_name):
            with pytest.raises(NotFound) as exc_info:
                self.client.request(cancel_order_endpoint)

            assert exc_info.value == "Object not found"

    def test_search_instruments(self):
        """
            Verify we can search for instruments
        """

        search_instruments_endpoint = SearchInstrument(keywords="AAPL")

        cassette_name = self.generate_cassette_name("search_instruments")
        with self.recorder.use_cassette(cassette_name):
            search_instruments_result = self.client.request(search_instruments_endpoint)

        logger.info(search_instruments_result)

        assert "Data" in search_instruments_result
        assert isinstance(search_instruments_result["Data"], list)

    def test_instrument_details(self):
        """
            Verify we can get details for specified instrument
        """

        search_instruments_endpoint = InstrumentDetails(uic=211, asset_type="Stock")

        cassette_name = self.generate_cassette_name("instrument_details")
        with self.recorder.use_cassette(cassette_name):
            instrument_details_result = self.client.request(search_instruments_endpoint)

        logger.info(instrument_details_result)

        assert instrument_details_result["PrimaryListing"] == 211
        assert instrument_details_result["AssetType"] == "Stock"

    def test_positions(self):
        """
            Verify we can get list of open positions
        """

        positions_endpoint = Positions()

        cassette_name = self.generate_cassette_name("positions")
        with self.recorder.use_cassette(cassette_name):
            positions_result = self.client.request(positions_endpoint)

        assert "__count" in positions_result
        assert "Data" in positions_result

    # def test_place_new_order(self):
    #     """
    #         Verify we can place new order
    #     """

    #     fx_order = {
    #         "AccountKey": os.getenv("SAXO__ACCOUNT_KEY"),
    #         "Amount": 10000,
    #         "AssetType": "FxSpot",
    #         "BuySell": "Sell",
    #         "OrderDuration": {
    #             "DurationType": "GoodTillCancel"
    #         },
    #         "OrderPrice": 1.13,
    #         "OrderType": "Limit",
    #         "Uic": 21
    #     }

    #     cassette_name = self.generate_cassette_name("place_new_order")
    #     with self.recorder.use_cassette(cassette_name):
    #         placed_order_result = self.client.place_new_order(fx_order)

    #     assert "data" in placed_order_result
    #     assert "OrderId" in placed_order_result["data"]

    # def test_fail_to_place_new_order(self):
    #     """
    #         Verify we can handle exception when placing new order
    #     """

    #     fx_order = {
    #         "AccountKey": os.getenv("SAXO__ACCOUNT_KEY"),
    #         "Amount": -1,
    #         "AssetType": "FxSpot",
    #         "BuySell": "Sell",
    #         "OrderDuration": {
    #             "DurationType": "GoodTillCancel"
    #         },
    #         "OrderPrice": 1.13,
    #         "OrderType": "Limit",
    #         "Uic": 21
    #     }

    #     cassette_name = self.generate_cassette_name("fail_to_place_new_order")
    #     with self.recorder.use_cassette(cassette_name):

    #         with pytest.raises(BadRequest) as exc_info:
    #             placed_order_result = self.client.place_new_order(fx_order)

    # def test_list_open_order(self):
    #     """
    #         Verify we can get list of open orders
    #     """

    #     cassette_name = self.generate_cassette_name("list_open_orders")
    #     with self.recorder.use_cassette(cassette_name):
    #         open_orders_result = self.client.get_open_orders()

    #     assert "data" in open_orders_result
    #     assert "Data" in open_orders_result["data"]
    #     assert len(open_orders_result["data"]["Data"]) > 0

    # def test_list_positions(self):
    #     """
    #         Verify we can get list of positions
    #     """

    #     cassette_name = self.generate_cassette_name("list_positions")
    #     with self.recorder.use_cassette(cassette_name):
    #         positions_result = self.client.get_positions()

    #     assert "data" in positions_result
    #     assert "Data" in positions_result["data"]
    #     assert len(positions_result["data"]["Data"]) > 0

    # def test_subscribe_to_streaming_prices_info(self):
    #     """
    #         Verify we can create price info subscription
    #     """

    #     _context_id = "test_priceinfo_context_id"
    #     _reference_id = "test_priceinfo_reference_id"

    #     cassette_name = self.generate_cassette_name("subscribe_to_streaming_price_info")
    #     with self.recorder.use_cassette(cassette_name):
    #         subs_result = self.client.subscribe_to_streaming_prices_info(context_id=_context_id,
    #                                                                      reference_id=_reference_id,
    #                                                                      uics=[23], asset_type="FxSpot")

    #     assert "data" in subs_result
    #     assert subs_result["data"]["ContextId"] == _context_id
    #     assert subs_result["data"]["ReferenceId"] == _reference_id

    # def test_subscribe_to_streaming_balance(self):
    #     """
    #         Verify we can create balance info subscription
    #     """

    #     _context_id = "test_balance_context_id"
    #     _reference_id = "test_balance_reference_id"

    #     cassette_name = self.generate_cassette_name("subscribe_to_balance_info")
    #     with self.recorder.use_cassette(cassette_name):
    #         subs_result = self.client.subscribe_to_streaming_balance(context_id=_context_id,
    #                                                                  reference_id=_reference_id)

    #     assert "data" in subs_result
    #     assert subs_result["data"]["ContextId"] == _context_id
    #     assert subs_result["data"]["ReferenceId"] == _reference_id
