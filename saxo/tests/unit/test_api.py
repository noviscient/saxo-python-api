import logging

import pytest

from saxo.api import SaxoAPI
from saxo.exceptions import BadEnvironment

logger = logging.getLogger("saxo.tests.unit")
logger.setLevel(level=logging.DEBUG)


def test_bad_environment_exception():
    with pytest.raises(BadEnvironment):
        SaxoAPI(
            client_key="",
            account_key="",
            access_token="",
            environment="Here is Wrong Environment",
        )


def test_none_access_token_exception():

    with pytest.raises(ValueError):
        SaxoAPI(
            access_token=None,
            client_key="test_client_key",
            account_key="test_account_key",
            environment="SIM",
        )


def test_authorization_header():
    token = "TEST TOKEN"
    client = SaxoAPI(
        access_token=token,
        client_key="test_client_key",
        account_key="test_account_key",
        environment="SIM",
    )

    assert "Authorization" in client.session.headers.keys()
    assert token in client.session.headers.get("Authorization")


# def test_wrong_http_method_exception(saxo_open_api_sim_client):

#     with pytest.raises(BadHTTPMethod):
#         saxo_open_api_sim_client._make_request(url="does not matter", method="ABC")


# def test_session_creation(saxo_open_api_sim_client):

#     assert saxo_open_api_sim_client.session is not None


# def test_missing_subscription_parameters(saxo_open_api_sim_client):

#     with pytest.raises(ValueError):
#         saxo_open_api_sim_client.subscribe_to_streaming_prices_info(context_id=None,
#                                                                     reference_id="test",
#                                                                     uics=[23],
#                                                                     asset_type="test")

#     with pytest.raises(ValueError):
#         saxo_open_api_sim_client.subscribe_to_streaming_prices_info(context_id="test",
#                                                                     reference_id=None,
#                                                                     uics=[23],
#                                                                     asset_type="test")

#     with pytest.raises(ValueError):
#         saxo_open_api_sim_client.subscribe_to_streaming_prices_info(context_id="test",
#                                                                     reference_id="test",
#                                                                     uics=None,
#                                                                     asset_type="test")

#     with pytest.raises(ValueError):
#         saxo_open_api_sim_client.subscribe_to_streaming_prices_info(context_id="test",
#                                                                     reference_id="test",
#                                                                     uics=[23],
#                                                                     asset_type=None)

#     with pytest.raises(ValueError):
#         saxo_open_api_sim_client.subscribe_to_streaming_balance(context_id=None,
#                                                                 reference_id="test")

#     with pytest.raises(ValueError):
#         saxo_open_api_sim_client.subscribe_to_streaming_balance(context_id="test",
#                                                                 reference_id=None)
